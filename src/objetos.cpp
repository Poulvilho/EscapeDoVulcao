#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

#include "objetos.hpp"

using namespace std;

Objetos :: Objetos(){

}


Objetos :: Objetos(char representacao, int posx, int posy){
	this->representacao = representacao;
	this->pos_x = posx;
	this->pos_y = posy;
}

void Objetos :: setPos_x(int valor){
	this->pos_x = valor;
}

void Objetos :: setPos_y(int valor){
	this->pos_y = valor;
}

void Objetos :: setRepresentacao(char representacao){
	this->representacao = representacao;
}

int Objetos :: getPos_x(){
	return this->pos_x;
}

int Objetos :: getPos_y(){
	return this->pos_y;
}

char Objetos :: getRepresentacao(){
	return this->representacao;
}

void Objetos :: movimento(){

	char direcao = 'l';

	direcao = getch();

	if(direcao == 'w'){
		this->setPos_y(-1);
	} else if (direcao == 's'){
		this->setPos_y(1);
	} else if (direcao == 'a'){
		this->setPos_x(-1);
	} else if (direcao == 'd'){
		this->setPos_x(1);
	}

}


