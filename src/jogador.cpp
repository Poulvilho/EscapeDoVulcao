#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

#include "jogador.hpp"
//#include "recorde.hpp"
//#include "demais.hpp"

using namespace std;


Jogador :: Jogador(){
	this->representacao = 'i';
	this->pos_x = 3;
	this->pos_y = 3;
	this->alive = 5;
	this->pontuacao = 0;
	this->winner = false;
}

void Jogador :: setPos_x(int valor){
	this->pos_x += valor;
}

void Jogador :: setPos_y(int valor){
	this->pos_y += valor;
}

void Jogador :: setRepresentacao(char representacao){
	this->representacao = representacao;
}

void Jogador :: setAlive(){
	this->alive--;
}

void Jogador :: setPontuacao(int valor){
	this->pontuacao += valor;
}

void Jogador :: setWinner(bool win){
	this->winner = TRUE;
}

int Jogador :: getPos_x(){
	return this->pos_x;
}

int Jogador :: getPos_y(){
	return this->pos_y;
}

char Jogador :: getRepresentacao(){
	return this->representacao;
}

int Jogador :: getAlive(){
	return this->alive;
}

int Jogador :: getPontuacao(){
	return this->pontuacao;
}

bool Jogador :: getWinner(){
	return this->winner;
}

void Jogador :: movimento(char w, char s, char a, char d){

	char direcao = 'l';

	direcao = getch();

	if(direcao == 'w'){
		if(w == '&'){}	else{
			this->setPos_y(-1);
		}
	} else if (direcao == 's'){
		if(s == '&'){}	else{
			this->setPos_y(1);
		}
	} else if (direcao == 'a'){
		if(a == '&'){}	else{
			this->setPos_x(-1);
		}
	} else if (direcao == 'd'){
		if(d == '&'){}	else{
			this->setPos_x(1);
		}
	}

}

int Jogador :: colisao(char **mapa, int posx, int posy){
	if(mapa[this->pos_x][this->pos_y] == '#'){
		printw("\n ups... ali não\n");
		return 1;
	}
	return 0;
}

/*void Jogador :: salvarJogador(Jogador* ronaldinho){		
	FILE * arq = fopen ("doc/Score.txt", "r");
	if(arq == NULL){
		arq = fopen("doc/Score.txt", "w");
		fprintf(arq, "ninguem\t0\nninguem\t0\nninguem\t0\nninguem\t0\nninguem\t0");
	
		delete(arq);
		FILE * arq = fopen ("doc/Score.txt", "r");
	}
	
	Recorde * recorde[7];
	char nome[10];
	int ponto, i;
	
	for(i=1; i<6; i++){
	fscanf (arq, "%s\t%d\n", nome, &ponto);
	recorde[i]->setNome(nome);
	recorde[i]->setPontos(ponto);
	}
	ponto = ronaldinho->getPontuacao();
	recorde[6]->setPontos(ponto);

	if(recorde[6]->getPontos() > recorde[5]->getPontos()){
		cout << "\tVocê bateu um recorde. Parabéns!!" << endl << endl;
		cout << "Digite seu nome com no exatos 10 caracteres: ";
		cin >> nome;
		recorde[6]->setNome(nome);
		recorde[5] = recorde[6];
		for(i=5; i>=1; i--){
			Recorde *aux = new Recorde;
			if(recorde[i]->getPontos() > recorde[i-1]->getPontos()){
				aux = recorde[i-1];
				recorde[i-1] = recorde[i];
				recorde[i] = aux;
			}
			delete(aux);
		}
		delete (arq);
		FILE *arq = fopen("doc/Score.txt", "w");
		for(i=1; i<6; i++){
			nome[10] = recorde[i]->getNome();
			ponto = recorde[i]->getPontos();
			fprintf(arq, "%s\t%d", nome, ponto);
		}

		delete(arq);
		delete (recorde[7]);
		placares();
	}
}
*/

