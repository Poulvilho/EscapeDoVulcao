#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>

#include "objetos.hpp"
#include "fogo.hpp"
#include "jogador.hpp"
#include "ponto.hpp"
#include "demais.hpp"
#include "mapa.hpp"
//#include "recorde.hpp"

using namespace std;

void mainMenu(){

	system("clear");

	char opcao;
	do{
		cout << "\t\tEscape do Vulcão" << endl <<endl;
		cout << "Aperte 1 e confirme para jogar!" << endl <<endl;
		//cout << "2- Placares" << endl;
		cout << "0- Sair do Jogo" << endl << endl;
		cin >> opcao;
		switch (opcao){
			case '1': jogar(); break;
			//case '2': system("clear"); placares(); break;
			case '0': break;
			default:
				system("clear");
				cout << "Digite uma opção válida" << endl << endl;
				break;
		}
	} while (opcao != '0');
}

void jogar(){

	Mapa * mapa = new Mapa();
	mapa->setRange();

	Jogador * ronaldinho = new Jogador();
	Fogo * fogo[20];
	for(int i = 0; i < 20; i++){
		fogo[i] = new Fogo();
		fogo[i]-> movimento();
		while(mapa->retornaElemento(fogo[i]->getPos_y(), fogo[i]->getPos_x()) == '&'){
			fogo[i]-> movimento();
		}
	}
	Ponto * ponto[5];
	for(int i = 0; i < 5; i++){
		ponto[i] = new Ponto();
		ponto[i]-> movimento();
		while(mapa->retornaElemento(ponto[i]->getPos_y(), ponto[i]->getPos_x()) == '&'){
			ponto[i]-> movimento();
		}
	}

	int tempo = 0;
	char teclado= 'a';

	while(TRUE){
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();

		//ADICIONA ARMADILHAS
		for(int i = 0; i < 20; i++){
			mapa->addElemento(fogo[i]->getRepresentacao(), fogo[i]->getPos_x(), fogo[i]->getPos_y());
			if(tempo == 4){
				fogo[i]->movimento();
				while(mapa->retornaElemento(fogo[i]->getPos_y(), fogo[i]->getPos_x()) == '&'){
					fogo[i]-> movimento();
				}
			}
		}

		//ADICIONAR PONTOS
		for(int i = 0; i < 5; i++){
			mapa->addElemento(ponto[i]->getRepresentacao(), ponto[i]->getPos_x(), ponto[i]->getPos_y());
			if(tempo == 4 ){
				ponto[i]->movimento();
				while(mapa->retornaElemento(ponto[i]->getPos_y(), ponto[i]->getPos_x()) == '&'){
					ponto[i]-> movimento();
				}
				tempo = 0;
			}
		}

		//ADICIONAR JOGADOR
		mapa->addElemento(ronaldinho->getRepresentacao(), ronaldinho->getPos_x(), ronaldinho->getPos_y());

		//IMPRIMIR MAPA
		printw("\tUltilize as teclas w, a, s, d para locomoção\n\n");
		mapa->getRange();

		tempo++;

		printw("Seu Personagem = %c, pos_x = %d, pos_y = %d\nvida = %d, Pontuação = %d, %c", ronaldinho->getRepresentacao(), ronaldinho->getPos_x(), ronaldinho->getPos_y(), ronaldinho->getAlive(), ronaldinho->getPontuacao(), mapa->retornaElemento(ronaldinho->getPos_y(),ronaldinho->getPos_x()));

		//MOVIMENTO
		ronaldinho->movimento(mapa->retornaElemento(ronaldinho->getPos_y() -1, ronaldinho->getPos_x()),
		mapa->retornaElemento(ronaldinho->getPos_y() +1, ronaldinho->getPos_x()),
		mapa->retornaElemento(ronaldinho->getPos_y(), ronaldinho->getPos_x() -1),
		mapa->retornaElemento(ronaldinho->getPos_y(), ronaldinho->getPos_x() +1));

		//COLISÃO COM AS ARMADILHAS
		for(int i = 0; i < 20; i++){
			if(ronaldinho->getPos_x() == fogo[i]->getPos_x() && ronaldinho->getPos_y() == fogo[i]->getPos_y()){
				ronaldinho->setAlive();
			}
		}
		//PERDER
		if(ronaldinho->getAlive() == 0){
			clear();
			mapa->YouLose();
			teclado = getch();
			endwin();
			break;
		}

		//PEGAR PONTO
		for(int i = 0; i < 5 ;i++){
			if(ronaldinho->getPos_x() == ponto[i]->getPos_x() && ronaldinho->getPos_y() == ponto[i]->getPos_y()){
				ronaldinho->setPontuacao(10);
			}
		}

		//GANHAR
		if(ronaldinho->getPos_x() == 49 && ronaldinho->getPos_y() == 16){
			clear();
			mapa->YouWin();
			printw("\tSua Pontuação: %d", ronaldinho->getPontuacao());
			
			//ronaldinho->salvarJogador(ronaldinho);

			teclado = getch();
			endwin();
			break;
		}

		//LIMPAR MAPA
		mapa->cleanRange();

		refresh();
		endwin();
	};

	delete(mapa);
	delete(ronaldinho);
	system("clear");
}

/*
void placares(){
	FILE *arq = fopen ("doc/Score.txt", "r");
	if (arq == NULL){
		cout << "\n\tNão há placares para mostrar" << endl;
		getchar();
		getchar();
		system("clear");
	}else{
		char nome[10];
		int pontuacao, contador=0;
		while (contador<5){
			fscanf(arq, "%s\t%d\n", nome, &pontuacao);
			cout << nome << ":\t" << pontuacao << endl;
			contador++;
		}
	fclose (arq);
	
	getchar();
	system("clear");
	}
	delete(arq);
}
*/
