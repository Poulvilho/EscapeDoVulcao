#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include <ctime>
#include <cstdlib>

#include "ponto.hpp"

using namespace std;


Ponto :: Ponto(){
	setRepresentacao('@');
	setPos_x(0);
	setPos_y(0);
	this->ponto = 1;
}

void Ponto :: setPonto(int ponto){
  this->ponto = ponto;
}

int Ponto :: getPonto(){
  return this->ponto;
}

void Ponto :: movimento() {
  int x, y;
  x = rand() % 50;
  y = rand() % 20;
  setPos_x(x);
  setPos_y(y);
}
