#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

#include "mapa.hpp"

using namespace std;

Mapa :: Mapa(){}

void Mapa :: setRange(){

	ifstream mapa ("doc/vulcao.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapa, aux);
		for(int u = 0; u < 50; u++){
			this->range[i][u] = aux[u];
			this->rangeB[i][u] = aux[u];
		}
	}

	mapa.close();
}

void Mapa :: getRange(){

		for(int i = 0; i < 20; i++){
			for(int u = 0; u < 50; u++){
				printw("%c", this->range[i][u]);
			}
		printw("\n");
		}
}

void Mapa :: cleanRange(){

	for(int i = 0; i < 20; i++){
		for(int u = 0; u < 50; u++){
			this->range[i][u] = this->rangeB[i][u];
		}
	}
}

void Mapa :: addElemento(char representacao, int posx, int posy){

	this->range[posy][posx] = representacao;

}

void Mapa :: YouLose(){
	ifstream mapalose ("doc/you_lose.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapalose, aux);
		for(int u = 0; u < 50; u++){
			this->youLose[i][u] = aux[u];
			printw("%c", this->youLose[i][u]);
		}
		printw("\n");
	}

	mapalose.close();
}

void Mapa :: YouWin(){
	ifstream mapawin ("doc/you_win.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapawin, aux);
		for(int u = 0; u < 50; u++){
			this->youWin[i][u] = aux[u];
			printw("%c", this->youWin[i][u]);
		}
		printw("\n");
	}

	mapawin.close();
}

char Mapa :: retornaElemento(int posx, int posy){
	return this->rangeB[posx][posy];
}
