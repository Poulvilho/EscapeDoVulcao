#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include <ctime>
#include <cstdlib>

#include "fogo.hpp"

using namespace std;


Fogo :: Fogo(){
	setRepresentacao('$');
	setPos_x(0);
	setPos_y(0);
	this->dano = 1;
}

void Fogo :: setDano(int dano){
	this->dano = dano;
}

int Fogo :: getDano(){
	return this->dano;
}

void Fogo :: movimento() {
	int x, y;
	x = rand() % 50;
	y = rand() % 20;
	setPos_x(x);
	setPos_y(y);
}


