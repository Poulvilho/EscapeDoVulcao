#ifndef RECORDE_HPP
#define RECORDE_HPP

class Recorde {
private:
	char nome[10];
	int pontos;
public:
	Recorde();
	void setNome(char nome[10]);
	char getNome();
	void setPontos(int pontos);
	int getPontos();

};

#endif
