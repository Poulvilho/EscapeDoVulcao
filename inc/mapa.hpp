#ifndef MAPA_HPP
#define MAPA_HPP

#include <iostream>
#include <string>

class Mapa {
private:
	char range[20][50];
	char rangeB[20][50];
	char youLose[20][50];
	char youWin[20][50];
public:
	Mapa();
	void setRange();
	void getRange();
	void YouLose();
	void YouWin();

	void cleanRange();
	void addElemento(char representacao, int posx, int posy);
	char retornaElemento(int posx, int posy);
};

#endif
