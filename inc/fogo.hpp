#ifndef FOGO_HPP
#define FOGO_HPP

#include <iostream>
#include <string>

#include "objetos.hpp"

class Fogo : public Objetos {
private:
	int dano;
public:
	Fogo();
	void setDano(int dano);
	int getDano();

	void movimento();

};

#endif
