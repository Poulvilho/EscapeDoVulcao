#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include <iostream>
#include <string>

#include "objetos.hpp"

class Jogador : public Objetos {
private:
	char representacao;
	int pos_x;
	int pos_y;
	int alive;
	int pontuacao;
	bool winner;
public:
	Jogador();
	void setPos_x(int valor);
	int getPos_x();
	void setPos_y(int valor);
	int getPos_y();
	void setRepresentacao(char Representacao);
	char getRepresentacao();
	void setAlive();
	int getAlive();
	void setPontuacao(int valor);
	int getPontuacao();
	void setWinner(bool win);
	bool getWinner();

	//void salvarJogador (Jogador* ronaldinho);
	void movimento(char w, char s, char a, char d);
	int colisao(char **mapa, int posx, int posy);
	
};

#endif
