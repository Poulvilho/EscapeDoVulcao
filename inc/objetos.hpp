#ifndef OBJETOS_HPP
#define OBJETOS_HPP

#include <iostream>
#include <string>

class Objetos{
private:
	char representacao;
	int pos_x;
	int pos_y;
public:
	Objetos();
	Objetos(char representacao, int posx, int posy);
	void setPos_x(int valor);
	int getPos_x();
	void setPos_y(int valor);
	int getPos_y();
	void setRepresentacao(char representacao);
	char getRepresentacao();

	void movimento();

};

#endif
