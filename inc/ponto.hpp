#ifndef PONTO_HPP
#define PONTO_HPP

#include <iostream>
#include <string>

#include "objetos.hpp"

class Ponto : public Objetos {
private:
	int ponto;
public:
	Ponto();
	int getPonto();
	void setPonto(int ponto);

	void movimento();

};

#endif
